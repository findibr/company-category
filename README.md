### Dependencies
Ionic
[Angular formly](https://github.com/formly-js/angular-formly)
[Angular formly ionic](https://github.com/formly-js/angular-formly-templates-ionic)
[window.plugins.toast](https://github.com/EddyVerbruggen/Toast-PhoneGap-Plugin)
```html
<script src="lib/ionic/js/ionic.bundle.js"></script>
<script src="lib/api-check/dist/api-check.min.js"></script>
<script src="lib/angular-formly/dist/formly.min.js"></script>
<script src="lib/angular-formly-templates-ionic/dist/angular-formly-templates-ionic.js"></script>
<script src="lib/company-category/company-categories-form.js"></script>
<script src="lib/company-category/types-list.js"></script>
```

### Insert module
```javascript
angular.module('myModule', ['findi.companyCategoriesForm'])
```

### Use
```html
<company-categories-form ng-model="myModel" options="inputOptions"></company-categories-form>
```
### inputOptions
```javascript
{
	configFields: {
		categoriesList: 'ARRAY_OF_OBJECT' // List of categories
		endpointSubcategory: 'STRING' // Endpoint to search for subcategories
		category: { // optional
			hide: 'BOOLEAN' // Remove input to form
			label: 'STRING' // Change label input
			labelProp: 'STRING' // Object key from the category list
			valueProp: 'STRING' // Object key from the category list
			key: 'STRING' // Change variable from model
		}
	},
	submit: function(data, rootCategoryId, form) { // required
		// data: myModel
		// form: form
	},
	submitButtonText: 'STRING' // Text button submit
}
```
