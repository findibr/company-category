(function() {
	'use strict'

	angular.module('findi.companyCategoriesForm')
		.directive('typesList', function() {
			return {
				template: '<ul class="types-list">' +
					'<li class="type-item" ng-class="{\'selected\': typeSelected.id == type.id}" ng-repeat="type in typesList">' +
					'<button type="button" ng-click="selectType(type)">{{type.name}}</button>' +
					'</li>' +
					'</ul>' +
					'<div ng-if="typeSelected.id && childrenList.length">' +
					'<h3 class="subtypes">{{options.subCategoryLabel}}{{typeSelected.name}}:</h3>' +
					'<types-list ng-model="ngModel" nivel="nivel+2" options="options" types-list="childrenList"></types-list>' +
					'</div>',
				restrict: 'E',
				scope: {
					ngModel: '=',
					typesList: '=',
					nivel: '=',
					options: '='
				},
				link: link,
				controller: controller
			}
		})

	function controller($scope, $http, $ionicLoading, $rootScope, $timeout) {
		$scope.getNewList = function(typeSelected) {
			$scope.childrenList = []
			$scope.typeSelected = typeSelected
			_fetchChildren(typeSelected)
		}

		$scope.bindNgModel = function(typeSelected) {
			var aux = $scope
			for (var i = 0; i < $scope.nivel; i++) {
				aux = aux['$parent']
			}
			aux.ngModel = typeSelected
		}

		$scope.selectType = function(type) {
			$scope.bindNgModel({})
			$timeout(function() {
				_resetScopeChildren()
				$scope.getNewList(type)
			})
		}

		var _fetchChildren = function(typeSelected) {
			$ionicLoading.show()
			$http.get($scope.options.endpointSubcategory + typeSelected.id).then(
				function(success) {
					$scope.childrenList = success.data

					if (!$scope.childrenList.length) {
						$scope.bindNgModel(typeSelected)
					}

					$timeout(function() {
						$rootScope.$emit('typeSelected', typeSelected)
					}, 100)
					$ionicLoading.hide()
				},
				function(error) {
					window.plugins.toast.show('Erro ao buscar tipos', 'short', 'bottom')
					$ionicLoading.hide()
				})
		}

		var _resetScopeChildren = function() {
			if ($scope.$$childTail && $scope.$$childTail.$$childTail) {
				$scope.$$childTail.$$childTail.childrenList = null
				$scope.$$childTail.$$childTail.typeSelected = null
			}
		}
	}

	function link($scope, element, attrs) {}
})()

