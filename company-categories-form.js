(function() {
	'use strict'

	angular.module('findi.companyCategoriesForm', ['ionic', 'formlyIonic'])
		.component('companyCategoriesForm', {
			template: '<form name="{{$ctrl.opts.formName}}" novalidate="novalidate" ng-submit="$ctrl.opts.submit($ctrl.categorySelected)">' +
				'<formly-form model="$ctrl.categorySelected" fields="$ctrl.opts.fields"></formly-form>' +
				'<div class="box-list-types">' +
				'<h3 ng-if="$ctrl.typesList.length">{{$ctrl.opts.typeName}}</h3>' +
				'<types-list ng-model="$ctrl.ngModel" types-list="$ctrl.typesList" nivel="0" options="$ctrl.options" ng-if="$ctrl.typesList.length"></types-list>' +
				'</div>' +
				'<input class="findi-btn" type="submit" value="{{$ctrl.opts.submitButtonText}}"/>' +
				'</form>',
			controller: companyCategoriesFormController,
			controllerAs: '$ctrl',
			bindings: {
				ngModel: '=',
				options: '='
			}
		})

	function companyCategoriesFormController($scope, formlyConfig, $http,
		$ionicLoading) {
		var vm = this
		vm.options = vm.options || {}
		var optionsParams = angular.copy(vm.options)
		vm.opts = _buildOpts(optionsParams)
		vm.options.subCategoryLabel = vm.options.subCategoryLabel || 'Subcategoria de '
		vm.objectIsEmpty = function(object) {
			return (!object || !Object.keys(object).length)
		}

		function _buildOpts(optionsParams) {
			var opts = {}
			opts.typeName = optionsParams.typeName || 'Tipo'
			opts.formName = "$ctrl.companyCategoriesForm"
			opts.configFields = optionsParams.configFields || {}
			opts.submitButtonText = optionsParams.submitButtonText || 'Continuar'
			opts.fields = _getFields(opts.configFields)
			opts.submit = function(data) {
				if (vm.objectIsEmpty(vm.ngModel)) {
					vm.companyCategoriesForm.$valid = false
					vm.companyCategoriesForm.$invalid = true
				}
				optionsParams.submit(vm.ngModel, vm.categorySelected.category, vm.companyCategoriesForm)
			}

			return opts
		}

		function _getFields(configFields) {
			var array = []
			var configFieldCategory = configFields.category || {}

			if (!configFieldCategory.hide) array.push(_getSelectCategoryField(
				configFieldCategory, $scope))

			return array
		}

		function _getSelectCategoryField(configField, scope) {
			return {
				key: configField.key ? configField.key : "category",
				type: "select",
				expressionProperties: {
					'templateOptions.options': 'model.categoriesList',
					'templateOptions.onChange': function(viewValue, modelValue, $scope) {
						if (viewValue) {
							_getTypes(viewValue)
						}
						_resetScopeChildren(scope)
						vm.ngModel = {}
					}
				},
				templateOptions: {
					required: true,
					options: [],
					valueProp: configField.valueProp ? configField.valueProp : 'id',
					labelProp: configField.labelProp ? configField.labelProp : 'name',
					label: configField.label ? configField.label : 'CATEGORIA'
				},
				controller: function($scope) {
					$scope.$watch(
						'$parent.$parent.$parent.$parent.$ctrl.options.categoriesList',
						function(value) {
							$scope.model.categoriesList = value
						})
				}
			}
		}

		function _getTypes(categoryId) {
			$ionicLoading.show()

			$http.get(optionsParams.endpointSubcategory + categoryId).then(function(
				success) {
				vm.typesList = success.data
				$ionicLoading.hide()
			}, function(error) {
				window.plugins.toast.show('Erro ao buscar tipos', 'short', 'bottom')
				$ionicLoading.hide()
			})
		}

		function _resetScopeChildren(scope) {
			if (scope.$$childTail && scope.$$childTail.$$childTail) {
				scope.$$childTail.$$childTail.typesList = null
				scope.$$childTail.$$childTail.typeSelected = null
			}
		}
	}
})()

